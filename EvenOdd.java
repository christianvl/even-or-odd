/* This is a simple "Even or Odd" game
by Christian Fairlie Pearson van Langendonck
on 2019.10.30
GPL3
*/
import java.util.Scanner;
import java.util.Random;

public class EvenOdd{

 public static void main(String[] args){

	 //variables
	 String choice1;
	 int numb1, numb2, numb3, turns, scorep1, scorep2;
	 scorep1 = 0;
	 scorep2 = 0;

	 //start scanner
	 Scanner input = new Scanner(System.in);

	 //start loop for turns
	 turns = 0;
	 for (turns = 0; scorep1 < 2 && scorep2 < 2; turns++) {
	 //even or odd selection
	 System.out.print("Type even or odd: ");
	 choice1 = input.next();
        while (!choice1.equals("even") && !choice1.equals("odd")) {
			 System.out.print("You must type even or odd: ");
			 choice1 = input.next();
		 }
		 if (choice1.equals("odd")){
			 System.out.println("Player is ODD and Computer is EVEN");
		 }
		 else {
			 System.out.println("Player is EVEN and Computer is ODD");
		 }

	 //Player 1 turn
     while (true) {
         System.out.print("Choose a number from 0 to 5: ");
         boolean hasNextInt = input.hasNextInt();
         if (hasNextInt) {
             numb1 = input.nextInt();
             if (numb1 <= 5 && numb1 >= 0) break;
             else System.out.print("You must choose a number from 0 to 5: ");
         }
         input.nextLine();
     }

	 //computer turn
	 Random rand = new Random();
	 numb2 = rand.nextInt(4);
	 System.out.println("Computer plays the number: " + numb2);

	 //Calculate and print result
	 numb3 = numb1 + numb2;

	 if (numb3 % 2 == 1){
		 System.out.println(numb3 + " is an ODD number");
		 if (choice1.equals("odd")) {
			 System.out.println("You win!");
			 scorep1++;
		 }
		 else {
			 System.out.println("You loose!");
			 scorep2++;
		 }
	 }
    else {
		 System.out.println(numb3 + " is an EVEN number");
		 if (choice1.equals("even")) {
			 System.out.println("You win!");
			 scorep1++;
		 }
		 else {
			 System.out.println("You loose!");
			 scorep2++;
		 }
	 }
    System.out.println("\nPlayer [ " + scorep1 + " ] vs. [ " + scorep2 + " ] Computer\n");
	 }// end of for loop

	 if (scorep1 > scorep2) {
        System.out.println("Congratulations! You win!\n");
        }
     else {
        System.out.println("Ha-ha! You loose!\n");
        }
	System.out.print("You've played " + turns + " turns");
}//end of main
}//end of class
