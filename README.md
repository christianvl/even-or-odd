# Even or Odd
<p>This is a simple java, console based, even or odd game. The player calls what they think the result will be (an even or an odd number). Both players choose simultaniously any number from 0 to 5(or 0 to ten). The numbers are added and the resulting number will set the winner (whoever picked even or odd accordingly).<p>

## License
<p>This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html).

